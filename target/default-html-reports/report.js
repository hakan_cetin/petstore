$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/petStore.feature");
formatter.feature({
  "name": "API Test",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Creating a pet by given information",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.step({
  "name": "User sends POST request to \"pet\"",
  "keyword": "When ",
  "rows": [
    {
      "cells": [
        "id",
        "\u003cid\u003e"
      ]
    },
    {
      "cells": [
        "name",
        "\u003cname\u003e"
      ]
    },
    {
      "cells": [
        "status",
        "\u003cstatus\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "id",
        "name",
        "status"
      ]
    },
    {
      "cells": [
        "2536",
        "dog1",
        "sold"
      ]
    },
    {
      "cells": [
        "7638",
        "dog2",
        "available"
      ]
    },
    {
      "cells": [
        "9767",
        "dog3",
        "pending"
      ]
    },
    {
      "cells": [
        "2763",
        "dog4",
        "sold"
      ]
    },
    {
      "cells": [
        "1967",
        "dog5",
        "sold"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Creating a pet by given information",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends POST request to \"pet\"",
  "rows": [
    {
      "cells": [
        "id",
        "2536"
      ]
    },
    {
      "cells": [
        "name",
        "dog1"
      ]
    },
    {
      "cells": [
        "status",
        "sold"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_POST_request_to(String,String,String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Creating a pet by given information",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends POST request to \"pet\"",
  "rows": [
    {
      "cells": [
        "id",
        "7638"
      ]
    },
    {
      "cells": [
        "name",
        "dog2"
      ]
    },
    {
      "cells": [
        "status",
        "available"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_POST_request_to(String,String,String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Creating a pet by given information",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends POST request to \"pet\"",
  "rows": [
    {
      "cells": [
        "id",
        "9767"
      ]
    },
    {
      "cells": [
        "name",
        "dog3"
      ]
    },
    {
      "cells": [
        "status",
        "pending"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_POST_request_to(String,String,String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Creating a pet by given information",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends POST request to \"pet\"",
  "rows": [
    {
      "cells": [
        "id",
        "2763"
      ]
    },
    {
      "cells": [
        "name",
        "dog4"
      ]
    },
    {
      "cells": [
        "status",
        "sold"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_POST_request_to(String,String,String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Creating a pet by given information",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends POST request to \"pet\"",
  "rows": [
    {
      "cells": [
        "id",
        "1967"
      ]
    },
    {
      "cells": [
        "name",
        "dog5"
      ]
    },
    {
      "cells": [
        "status",
        "sold"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_POST_request_to(String,String,String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Getting pet information by id",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.step({
  "name": "User sends GET request to \"pet/\"\u003cid\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies that response status code is \u003cstatusCode\u003e",
  "keyword": "Then "
});
formatter.step({
  "name": "User verifies that response content type is \"\u003ccontentType\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "id",
        "statusCode",
        "contentType"
      ]
    },
    {
      "cells": [
        "7638",
        "200",
        "application/json"
      ]
    },
    {
      "cells": [
        "5702",
        "404",
        "application/json"
      ]
    },
    {
      "cells": [
        "2763",
        "200",
        "application/json"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Getting pet information by id",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends GET request to \"pet/\"7638",
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_GET_request_to(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Getting pet information by id",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends GET request to \"pet/\"5702",
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_GET_request_to(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 404",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Getting pet information by id",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends GET request to \"pet/\"2763",
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_GET_request_to(String,int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Finding all pets by status",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.step({
  "name": "User sends a GET request to \"pet/findByStatus\"",
  "keyword": "And "
});
formatter.step({
  "name": "User selects pets with \"\u003cstatus\u003e\"",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "And "
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "status"
      ]
    },
    {
      "cells": [
        "sold"
      ]
    },
    {
      "cells": [
        "pending"
      ]
    },
    {
      "cells": [
        "available"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Finding all pets by status",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends a GET request to \"pet/findByStatus\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_GET_request_to(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User selects pets with \"sold\"",
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_selects_pets_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Finding all pets by status",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends a GET request to \"pet/findByStatus\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_GET_request_to(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User selects pets with \"pending\"",
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_selects_pets_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Finding all pets by status",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends a GET request to \"pet/findByStatus\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_GET_request_to(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User selects pets with \"available\"",
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_selects_pets_with(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Deleting an existing pet with API key",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.step({
  "name": "User sends DELETE request to \"pet/\"\u003cid\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "Select \"special-key\" for the authorization filters as a API key",
  "keyword": "When "
});
formatter.step({
  "name": "User verifies that response status code is \u003cstatusCode\u003e",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "id",
        "statusCode"
      ]
    },
    {
      "cells": [
        "7638",
        "200"
      ]
    },
    {
      "cells": [
        "9076",
        "404"
      ]
    },
    {
      "cells": [
        "2763",
        "200"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Deleting an existing pet with API key",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends DELETE request to \"pet/\"7638",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_DELETE_request_to(String,Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Select \"special-key\" for the authorization filters as a API key",
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.select_for_the_authorization_filters_as_a_API_key(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Deleting an existing pet with API key",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends DELETE request to \"pet/\"9076",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_DELETE_request_to(String,Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Select \"special-key\" for the authorization filters as a API key",
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.select_for_the_authorization_filters_as_a_API_key(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 404",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Deleting an existing pet with API key",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends DELETE request to \"pet/\"2763",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_DELETE_request_to(String,Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Select \"special-key\" for the authorization filters as a API key",
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.select_for_the_authorization_filters_as_a_API_key(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Placing orders for a pet",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.step({
  "name": "User sends POST request to as an \"store/order\"",
  "keyword": "When ",
  "rows": [
    {
      "cells": [
        "petId",
        "\u003cpetId\u003e"
      ]
    },
    {
      "cells": [
        "quantity",
        "\u003cquantity\u003e"
      ]
    },
    {
      "cells": [
        "status",
        "\u003cstatus\u003e"
      ]
    },
    {
      "cells": [
        "complete",
        "\u003ccomplete\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "petId",
        "quantity",
        "status",
        "complete"
      ]
    },
    {
      "cells": [
        "1567",
        "2",
        "placed",
        "true"
      ]
    },
    {
      "cells": [
        "2340",
        "1",
        "approved",
        "true"
      ]
    },
    {
      "cells": [
        "3109",
        "3",
        "delivered",
        "true"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Placing orders for a pet",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends POST request to as an \"store/order\"",
  "rows": [
    {
      "cells": [
        "petId",
        "1567"
      ]
    },
    {
      "cells": [
        "quantity",
        "2"
      ]
    },
    {
      "cells": [
        "status",
        "placed"
      ]
    },
    {
      "cells": [
        "complete",
        "true"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_POST_request_to_as_an(String,String,String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Placing orders for a pet",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends POST request to as an \"store/order\"",
  "rows": [
    {
      "cells": [
        "petId",
        "2340"
      ]
    },
    {
      "cells": [
        "quantity",
        "1"
      ]
    },
    {
      "cells": [
        "status",
        "approved"
      ]
    },
    {
      "cells": [
        "complete",
        "true"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_POST_request_to_as_an(String,String,String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Placing orders for a pet",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@APITest"
    }
  ]
});
formatter.step({
  "name": "Headers accepts content type as \"application/json\"",
  "keyword": "Given "
});
formatter.match({
  "location": "petStoreStepDefinitions.headers_accepts_content_type_as(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User sends POST request to as an \"store/order\"",
  "rows": [
    {
      "cells": [
        "petId",
        "3109"
      ]
    },
    {
      "cells": [
        "quantity",
        "3"
      ]
    },
    {
      "cells": [
        "status",
        "delivered"
      ]
    },
    {
      "cells": [
        "complete",
        "true"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_sends_POST_request_to_as_an(String,String,String\u003e)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response status code is 200",
  "keyword": "Then "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_status_code_is(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User verifies that response content type is \"application/json\"",
  "keyword": "And "
});
formatter.match({
  "location": "petStoreStepDefinitions.user_verifies_that_response_content_type_is(String)"
});
formatter.result({
  "status": "passed"
});
});