@APITest
Feature: API Test

# To test the API it was created 5 data with POST request
  Scenario Outline: Creating a pet by given information
    Given Headers accepts content type as "application/json"
    When User sends POST request to "pet"
      | id     | <id>     |
      | name   | <name>   |
      | status | <status> |
    Then User verifies that response status code is 200
    And User verifies that response content type is "application/json"
    Examples:
      | id   | name | status    |
      | 2536 | dog1 | sold      |
      | 7638 | dog2 | available |
      | 9767 | dog3 | pending   |
      | 2763 | dog4 | sold      |
      | 1967 | dog5 | sold      |

# The data created beforehand was fetched  with GET request
# And also here conducted negative test
  Scenario Outline: Getting pet information by id
    Given Headers accepts content type as "application/json"
    When User sends GET request to "pet/"<id>
    Then User verifies that response status code is <statusCode>
    And User verifies that response content type is "<contentType>"
    Examples:
      | id   | statusCode | contentType      |
      | 7638 | 200        | application/json |
      | 5702 | 404        | application/json |
      | 2763 | 200        | application/json |


# Pets with status were fetched with GET request
  Scenario Outline: Finding all pets by status
    Given Headers accepts content type as "application/json"
    And User sends a GET request to "pet/findByStatus"
    When User selects pets with "<status>"
    And  User verifies that response status code is 200
    Then User verifies that response content type is "application/json"
    Examples:
      | status    |
      | sold      |
      | pending   |
      | available |


# The data created beforehand was deleted with DELETE request
# And also here conducted negative test
  Scenario Outline: Deleting an existing pet with API key
    Given Headers accepts content type as "application/json"
    And User sends DELETE request to "pet/"<id>
    When Select "special-key" for the authorization filters as a API key
    Then User verifies that response status code is <statusCode>

    Examples:
      | id   | statusCode |
      | 7638 | 200        |
      | 9076 | 404        |
      | 2763 | 200        |


#
  Scenario Outline: Placing orders for a pet
    Given Headers accepts content type as "application/json"
    When User sends POST request to as an "store/order"
        | petId      | <petId>  |
        | quantity   |<quantity>|
        | status     | <status> |
        |complete    |<complete>|
    Then User verifies that response status code is 200
    And User verifies that response content type is "application/json"
    Examples:
      | petId | quantity | status    | complete |
      | 1567  | 2        | placed    | true     |
      | 2340  | 1        | approved  | true     |
      | 3109  | 3        | delivered | true     |

