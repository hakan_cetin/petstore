
package com.petstore.pojos.pets;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category implements Serializable
{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    private final static long serialVersionUID = -6197066823207771628L;

    public Category(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

}
