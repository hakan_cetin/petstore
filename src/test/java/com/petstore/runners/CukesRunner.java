package com.petstore.runners;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {
                "pretty",
                "json:target/cucumber.json", "html:target/default-html-reports", "rerun:target/rerun.txt"
        },

        features = "src/test/resources/features",
        glue = "com/petstore/steps",
        dryRun = false,
        tags = "@APITest"
)

public class CukesRunner {

}
