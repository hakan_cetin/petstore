

PROJECT

This project has been designed in accordance with BDD Serenity Cucumber Framework. 

FRAMEWORK

Cucumber is a tool for running automated acceptance tests written in a behavior driven development style. One of its wondrous main features is its ability to execute plain text functional description (written in language named Gherkin) as automated tests.
In the framework, Maven was used as a build tool. Third party libraries (dependencies) were added to pom.xml. 

SCENARIOS

In this project, Scenario Outline (for similar data trace) was utilized in which test cases can be executed multiple times depending upon the data provided as Example.

1-Creating a pet by given information:
In this scenario, the test framework sends POST request to create new pet records. Created records are selected from multi-data options. This DDT execution provides easier implementation. With Scenario Outline, 5 different pet records were created. 
This scenario checks whether POST requests are implemented properly by the API server.
 
2-Getting pet information by id/3-Finding all pets by status:
These test cases provide to check which records can be reachable from the API server. In these scenarios, both positive and negative test cases are designed to verify the proper working of the API server. 
To test the GET requests, DDT techniques are used as well. 
        
4-Deleting an existing pet with API key:
These test cases test DELETE request, whether they are implemented properly as well as the API server responses as expected. Both positive and negative scenarios are implemented for checking status codes.
API server response body is checked to verify the content's structure. All endpoint interaction works properly.

5-Finding all pets by status:
In these test cases, new orders are implemented by using POST request to check whether the API server works as expected. 

    
REPORTING

Cucumber serenity framework produces a fancy report by using Maven. This HTML report shows details of the test results as well as their steps. 

In the assignment, all 17 Test Scenarios have passed.
    
It can be seen in this report how many scenarios run, and how many of them failed and passed in any given time. All existing reports can be found under the target file. All reports are created by the command from CukesRunner class inside Cucumber options.
While executing the test, Cucumber framework provides an HTML report as a default under the target file.
- target/site/serenity/index.html
Also by the running Maven Life Cycle (mvn test or mvn verify commands), cucumber creates a fancy HTML-JSON Cucumber reports by the help of a maven plugin which is added before to pom.xml.
